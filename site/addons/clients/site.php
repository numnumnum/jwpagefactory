<?php
/**
 * @author       JoomWorker
 * @email        info@joomla.work
 * @url          http://www.joomla.work
 * @copyright    Copyright (c) 2010 - 2019 JoomWorker
 * @license      GNU General Public License version 2 or later
 * @date         2019/01/01 09:30
 */
//no direct accees
defined('_JEXEC') or die ('Restricted access');

class JwpagefactoryAddonClients extends JwpagefactoryAddons
{

	public function render()
	{
		$settings = $this->addon->settings;
		$class = (isset($settings->class) && $settings->class) ? $settings->class : '';
		$title = (isset($settings->title) && $settings->title) ? $settings->title : '';
		$alignment = (isset($settings->alignment) && $settings->alignment) ? $settings->alignment : '';
		$columns = (isset($settings->count) && $settings->count) ? $settings->count : 2;
		$heading_selector = (isset($settings->heading_selector) && $settings->heading_selector) ? $settings->heading_selector : 'h3';

		$output = '';
		$output = '<div class="jwpf-addon jwpf-addon-clients ' . $alignment . ' ' . $class . '">';

		if ($title) {
			$output .= '<' . $heading_selector . ' class="jwpf-addon-title">' . $title . '</' . $heading_selector . '>';
		}

		$output .= '<div class="jwpf-addon-content">';
		$output .= '<div class="jwpf-row">';

		foreach ($settings->jw_clients_item as $key => $value) {
			if ($value->image) {
				$output .= '<div class="' . $columns . '">';
				if (isset($value->url) && $value->url) $output .= '<a target="_blank" rel="nofollow" href="' . $value->url . '">';
				$output .= '<img class="jwpf-img-responsive" src="' . $value->image . '" alt="' . $value->title . '">';
				if (isset($value->url) && $value->url) $output .= '</a>';
				$output .= '</div>';
			}
		}

		$output .= '</div>';
		$output .= '</div>';
		$output .= '</div>';

		return $output;
	}

	public static function getTemplate()
	{
		$output = '
		<div class="jwpf-addon jwpf-addon-clients {{ data.class }} {{ data.alignment }}">
			<# if( !_.isEmpty( data.title ) ){ #><{{ data.heading_selector }} class="jwpf-addon-title jw-inline-editable-element" data-id={{data.id}} data-fieldName="title" contenteditable="true">{{ data.title }}</{{ data.heading_selector }}><# } #>
			<div class="jwpf-addon-content">
				<div class="jwpf-row">
					<# _.each(data.jw_clients_item, function(clients_item, key){ #>
						<# if(clients_item.image){ #>
							<div class="{{ data.count }}">
								<# if(clients_item.url){ #><a target="_blank" rel="nofollow" href=\'{{clients_item.url}}\'><# } #>
									<# if(clients_item.image && clients_item.image.indexOf("https://") == -1 && clients_item.image.indexOf("http://") == -1){ #>
										<img class="jwpf-img-responsive" src=\'{{ pagefactory_base + clients_item.image }}\' alt="{{ clients_item.title }}">
									<# } else if(clients_item.image){ #>
										<img class="jwpf-img-responsive" src=\'{{ clients_item.image }}\' alt="{{ clients_item.title }}">
									<# } #>
								<# if(clients_item.url){ #></a><# } #>
							</div>
						<# } #>
					<# }); #>
				</div>
			</div>
		</div>
		';

		return $output;
	}
}
